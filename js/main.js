var studentList = [];

const STUDENT_LIST = 'studentList';

// LocalStorage
var studentListJSON = localStorage.getItem(STUDENT_LIST);
if (studentListJSON !== null) {
  let studentArr = JSON.parse(studentListJSON);
  studentList = studentArr.map(function (item) {
    return new Student(
      item.code,
      item.name,
      item.email,
      item.password,
      item.birthday,
      item.course,
      item.mathScore,
      item.physicScore,
      item.chemistryScore
    );
  });
  render(studentList);
}

function addStudent() {
  let studentValue = getValueFromForm();

  studentList.push(studentValue);

  //   LocalStorage
  saveToLocalStorage(studentList, STUDENT_LIST);
  //   Rendering interface
  render(studentList);
}

function deleteStudent(id) {
  let location = searchCodeStudent(id, studentList);
  if (location != -1) {
    studentList.splice(location, 1);
    saveToLocalStorage(studentList, STUDENT_LIST);
    render(studentList);
  }
}

function resetForm() {
  document.getElementById('txtMaSV').value = '';
  document.getElementById('txtTenSV').value = '';
  document.getElementById('txtEmail').value = '';
  document.getElementById('txtPass').value = '';
  document.getElementById('txtNgaySinh').value = '';
  document.getElementById('khSV').value = 'Chọn khóa học';
  document.getElementById('txtDiemToan').value = '';
  document.getElementById('txtDiemLy').value = '';
  document.getElementById('txtDiemHoa').value = '';
}

function editStudent(id) {
  let location = searchCodeStudent(id, studentList);

  if (location === -1) {
    return;
  }

  let student = studentList[location];

  showStudent(student);
}

function updateStudent() {
  let student = getValueFromForm();
  let location = searchCodeStudent(student.code, studentList);

  if (location != -1) {
    studentList[location] = student;
    saveToLocalStorage(studentList, STUDENT_LIST);
    render(studentList);
  }
}
