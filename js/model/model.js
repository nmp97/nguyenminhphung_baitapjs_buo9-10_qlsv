function Student(
  _code,
  _name,
  _email,
  _password,
  _birthday,
  _course,
  _mathScore,
  _physicScore,
  _chemistryScore
) {
  this.code = _code;
  this.name = _name;
  this.email = _email;
  this.password = _password;
  this.birthday = _birthday;
  this.course = _course;
  this.mathScore = _mathScore;
  this.physicScore = _physicScore;
  this.chemistryScore = _chemistryScore;
  this.averageScore = function () {
    return ((_mathScore + _physicScore + _chemistryScore) / 3).toFixed(1);
  };
}
