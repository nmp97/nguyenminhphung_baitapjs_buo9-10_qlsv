function getValueFromForm() {
  let _code = document.getElementById('txtMaSV').value;
  let _name = document.getElementById('txtTenSV').value;
  let _email = document.getElementById('txtEmail').value;
  let _password = document.getElementById('txtPass').value;
  let _birthday = document.getElementById('txtNgaySinh').value;
  let _course = document.getElementById('khSV').value;
  let _mathScore = document.getElementById('txtDiemToan').value * 1;
  let _physicScore = document.getElementById('txtDiemLy').value * 1;
  let _chemistryScore = document.getElementById('txtDiemHoa').value * 1;

  let student = new Student(
    _code,
    _name,
    _email,
    _password,
    _birthday,
    _course,
    _mathScore,
    _physicScore,
    _chemistryScore
  );

  return student;
}

function render(arr) {
  var contentHTML = '';
  for (var index = 0; index < arr.length; index++) {
    var item = arr[index];
    var contentTr = `<tr>
                        <td>${item.code}</td>
                        <td>${item.name}</td>
                        <td>${item.email}</td>
                        <td>${item.birthday}</td>
                        <td>${item.course}</td>
                        <td>${item.averageScore()}</td>
                        <td>
                          <div class="row">
                            <div class='col-6 px-0'>
                              <button class='btn btn-danger' onclick="deleteStudent('${
                                item.code
                              }')">Xóa</button>
                            </div>
                            <div class='col-6 px-0'>
                              <button class='btn btn-primary' onclick="editStudent('${
                                item.code
                              }')">Sửa</button>
                            </div>
                          </div>
                        </td>
                    </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById('tbodySinhVien').innerHTML = contentHTML;
}

function searchCodeStudent(id, arr) {
  let location = -1;

  for (var index = 0; index < arr.length; index++) {
    let locationStudent = arr[index];
    if (locationStudent.code == id) {
      location = index;
      break;
    }
  }

  return location;
}

function showStudent(arr) {
  document.getElementById('txtMaSV').value = arr.code;
  document.getElementById('txtTenSV').value = arr.name;
  document.getElementById('txtEmail').value = arr.email;
  document.getElementById('txtPass').value = arr.password;
  document.getElementById('txtNgaySinh').value = arr.birthday;
  document.getElementById('khSV').value = arr.course;
  document.getElementById('txtDiemToan').value = arr.mathScore;
  document.getElementById('txtDiemLy').value = arr.physicScore;
  document.getElementById('txtDiemHoa').value = arr.chemistryScore;
}

function saveToLocalStorage(arr, key) {
  let studentListJSON = JSON.stringify(arr);
  localStorage.setItem(key, studentListJSON);
}
